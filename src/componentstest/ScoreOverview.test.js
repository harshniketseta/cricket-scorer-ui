import React from 'react';
import ReactDOM from 'react-dom';
import ScoreOverview from '../components/ScoreOverview'
import { shallow, mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import configureStore from 'redux-mock-store'


configure({ adapter: new Adapter() });

const initialState = {
   game :{ teams: [
        {
            id: 1,
            name: 'India',
            score: 130,
            wickets: 4
        },
        {
            id: 2,
            name: 'Pakistan',
            score: 20,
            wickets: 1
        }
    ],
    innings: [
        {
            id: 1,
            battingTeam: 1,
            bowlingTeam: 2,
            oversPlayed: 10,
        },
        {
            id: 2,
            battingTeam: 2,
            bowlingTeam: 1,
            oversPlayed: 0,
        }
    ],
    game: {
        winner: -1,
        inning1: 1,
        inning2: 2,
        currentInning: 1,
        isFirstInning : false,
        totalOvers: 20
    }
}};

const mockStore = configureStore();
let wrapper;
let store;



describe('ScoreOverview', () => {
    beforeEach(() => {
        //creates the store with any initial state or middleware needed  
        
        store = mockStore(initialState);
        console.log(store.getState());
        wrapper = shallow(<ScoreOverview store={store}/>);
        
    })

    it("validates the output",()=>{
        expect(wrapper.html().indexOf('.score-overview').length>-1);
    })

    it("validates team india present",()=>{
        let innings1Team =  initialState.game.teams[0];
        let result = `Team ${innings1Team.name}`;
        expect(wrapper.html().indexOf(result).length>-1);
    })

    it("validates team india score",()=>{
        let innings1Team =  initialState.game.teams[0];
        let result = `${innings1Team.score}/${innings1Team.wickets}`;
        expect(wrapper.html().indexOf('130/4').length>-1);
    })

    it("validates team Paksitan not present if its first innings",()=>{
        let innings2Team =  initialState.game.teams[1];
        let teamName = innings2Team.name;
        expect(wrapper.html().indexOf(teamName).length==-1);
    })

  });