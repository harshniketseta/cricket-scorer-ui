import React from 'react';
import ReactDOM from 'react-dom';
import {shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {CurrentOverInfo} from "../components/CurrentOverInfo.js";
import {Bye, LegBye, NoBall, Out, Wide} from "../appConstants";

configure({adapter: new Adapter()});
console.log(CurrentOverInfo);
describe('<CurrentOverInfo />', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<CurrentOverInfo currentOverInfo={[[1], [1, NoBall], [4, Wide], [1, Out], [2, LegBye], [2, Bye]]}/>, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    it('renders current over correctly', () => {
        const overInfo = shallow(<CurrentOverInfo currentOverInfo={[[1], [1, NoBall], [4, Wide], [1, Out], [2, LegBye], [2, Bye]]}/>);
        console.log("Over info - ", overInfo.html());
        expect(overInfo.html()).toEqual(
            expect.stringContaining('1 1N 4W 1Out 2Lb 2B')
        );
    });
});
