import React from 'react';
import ReactDOM from 'react-dom';
import {CurrentBowlerInfo} from '../components/CurrentBowlerInfo';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });
console.log(CurrentBowlerInfo);
describe('<CurrentBowlerInfo />', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<CurrentBowlerInfo />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders row div', () => {
    const bowlerInfo = shallow(<CurrentBowlerInfo />);
    expect(bowlerInfo.find('.row').length).toEqual(1);
  })

  it('renders current bowler name correctly', () => {
    const bowlerInfo = shallow(<CurrentBowlerInfo currentBowler="Sehwag"/>);
    expect(bowlerInfo.html().indexOf("Sehwag")>-1);
  });

});

