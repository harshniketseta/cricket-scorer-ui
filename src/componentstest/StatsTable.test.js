import React from 'react';
// import ReactDOM from 'react-dom';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import configureStore from 'redux-mock-store';

import {StatsTable} from '../components/StatsTable';
import {Bye, LegBye, NoBall, Out, Wide} from "../appConstants";

configure({adapter: new Adapter()});

const initialState = {
    teams: [
        {
            id: 1,
            name: 'India',
            isBatting: false,
            isBowling: true,
            score: 130,
            wickets: 4,
            players: [
                {
                    id: 1,
                    name: 'T1-P1',
                    isBowler: false,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0,
                    hasBowled: true,
                },
                {
                    id: 2,
                    name: 'T1-P2',
                    isBowler: true,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0,
                    hasBowled: true,
                },
                {
                    id: 3,
                    name: 'T1-P3',
                    isBowler: true,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0
                },
                {
                    id: 4,
                    name: 'Harsh',
                    isBowler: true,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0
                },
                {
                    id: 5,
                    name: 'Radha',
                    isBowler: false,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0
                },
                {
                    id: 6,
                    name: 'Ashish',
                    isBowler: false,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0
                },
                {
                    id: 7,
                    name: 'Chirag',
                    isBowler: false,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0
                },
                {
                    id: 8,
                    name: 'Quazi',
                    isBowler: true,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0
                },
                {
                    id: 9,
                    name: 'Piyush',
                    isBowler: false,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0
                },
                {
                    id: 10,
                    name: 'Mukarram',
                    isBowler: true,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0
                },
                {
                    id: 11,
                    name: 'Mrinmoy',
                    isBowler: true,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0
                }
            ],
        },
        {
            id: 2,
            name: 'Pakistan',
            score: 20,
            isBatting: true,
            isBowling: false,
            wickets: 1,
            players: [
                {
                    id: 12,
                    name: 'T2-P1',
                    isBowler: true,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0,
                    onCrease: true,
                    hasBatted: true,
                    hasBowled: true,
                },
                {
                    id: 13,
                    name: 'T2-P2',
                    isBowler: true,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0,
                    onCrease: true,
                    hasBatted: true,
                    hasBowled: true,
                },
                {
                    id: 14,
                    name: 'T2-P3',
                    isBowler: true,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0,
                    hasBatted: true,
                    hasBowled: true,
                },
                {
                    id: 15,
                    name: 'Guru',
                    isBowler: true,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0
                },
                {
                    id: 16,
                    name: 'Vinay',
                    isBowler: false,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0
                },
                {
                    id: 17,
                    name: 'Kaustubh',
                    isBowler: false,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0
                },
                {
                    id: 18,
                    name: 'Mayur',
                    isBowler: false,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0
                },
                {
                    id: 19,
                    name: 'Rahul',
                    isBowler: true,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0
                },
                {
                    id: 20,
                    name: 'Ajit',
                    isBowler: false,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0
                },
                {
                    id: 21,
                    name: 'Amir',
                    isBowler: true,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0
                },
                {
                    id: 22,
                    name: 'Shahrukh',
                    isBowler: true,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0
                }
            ],
        }
    ],
    innings: [
        {
            id: 1,
            battingTeam: 1,
            bowlingTeam: 2,
            playersOnCrease: [1, 2],
            currentBatsman: 1,
            currentBowler: 14,
            currentOverState: [[1] , [1, NoBall], [4, Wide], [1, Out], [2, LegBye], [2, Bye]],
            oversPlayed: 20,
        },
        {
            id: 2,
            battingTeam: 2,
            bowlingTeam: 1,
            playersOnCrease: [12, 13],
            currentBatsman: 12,
            currentBowler: 4,
            currentOverState: [[1], [2], [2]],
            oversPlayed: 3,
        }
    ],
    game: {
        winner: -1,
        inning1: 1,
        inning2: 2,
        currentInning: 2,
        isFirstInning : false,
        totalOvers: 20,
    },
    route:"main"
};
const mockStore = configureStore();
// let wrapper;
let store;
let battingTeam;
let bowlingTeam;

describe('This has the test cases for Stats Table.', () => {

    beforeEach(() => {

        store = mockStore(initialState);
    });
 

  it('Renders without crashing for batting table with HEADER.', () => {
    setTeams(0,1);

    const statsTable = shallow(<StatsTable 
        store={store}
        title="Batting Table" 
        team={getBattingTeam()} 
        players={getBattingTeam().players} 
        columns={getBattingStatColumns()} 
        keys={getBattingStatKeys()}
        />);

    const expectedOutput = "<th scope=\"col\">Batsman</th><th scope=\"col\">Runs</th><th scope=\"col\">Balls</th><th scope=\"col\">Fours</th><th scope=\"col\">Sizes</th><th scope=\"col\">Strike Rate</th>";
    const acutalOutput = statsTable.html();

    expect(acutalOutput.indexOf(expectedOutput).length>-1);//.toEqual(expectedOutput);
  });

  it('renders without crashing for Bowling table, with BOWLER data.', () => {
    setTeams(0,1);

    const statsTable = shallow(<StatsTable 
        store={store}
        title="Bowling Table" 
        team={getBowlingTeam()} 
        players={getPlayersBowled()} 
        columns={getBowlingStatColumns()} 
        keys={getBowlingStatKeys()}
        />);

    const expectedOutput = "<div class=\"row\"><div class=\"col-lg-12 col-md-12\"><h5 class=\"card-title\">Bowling Table</h5><table class=\"table\"><thead><tr><th scope=\"col\">Bowler</th><th scope=\"col\">Overs</th><th scope=\"col\">Maiden</th><th scope=\"col\">Run</th><th scope=\"col\">Wickets</th></tr></thead><tbody><tr><th scope=\"row\">T2-P1<sup>*</sup></th><td>0</td><td>0</td><td>0</td><td>0</td></tr><tr><th scope=\"row\">T2-P2<sup>*</sup></th><td>0</td><td>0</td><td>0</td><td>0</td></tr><tr><td>T2-P3</td><td>0</td><td>0</td><td>0</td><td>0</td></tr></tbody></table></div></div>";
    const acutalOutput = statsTable.html();

    expect(acutalOutput.indexOf(expectedOutput).length>-1);//.toEqual(expectedOutput);
  });

    const getBattingStatColumns = () => {
        return ["Batsman", "Runs", "Balls", "Fours", "Sizes", "Strike Rate"];
    }
    const getBattingStatKeys = () => {
        return ["name", "runsTaken", "ballsPlayed", "foursPlayed", "sixPlayed", "strikeRate"];
    }
    const getBattingTeam = () => {
        return battingTeam;
    }
    const getBowlingTeam = () => {
        return bowlingTeam;
    }
    const getPlayersBowled = () => {
        return getBowlingTeam().players.filter((player) => player.hasBowled );
    }
    const getBowlingStatColumns = () => {
        return ["Bowler", "Overs", "Maiden", "Run", "Wickets"];
    }
    const getBowlingStatKeys = () => {
        return ["name", "oversBowled", "maidens", "runsGiven", "wickets"];
    }

  const setTeams = (battingIndex, bowlingIndex) => {
    battingTeam = store.getState().teams[battingIndex];
    bowlingTeam = store.getState().teams[bowlingIndex];
  };

});


