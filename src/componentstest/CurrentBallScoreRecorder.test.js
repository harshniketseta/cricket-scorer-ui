import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import CurrentBallsScoreRecorder from "../components/CurrentBallsScoreRecorder.js";
import configureStore from 'redux-mock-store'

configure({ adapter: new Adapter() });

const initialData = {game:{
    teams: [
        {
            id: 1,
            name: 'India',
            isBatting: false,
            isBowling: true,
            score: 130,
            wickets: 4,
            players: [],
        },
        {
            id: 2,
            name: 'Pakistan',
            score: 20,
            isBatting: true,
            isBowling: false,
            wickets: 1,
            players: [
                {
                    id: 12,
                    name: 'Abhijit',
                    isBowler: true,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0,
                    onCrease: true,
                    hasBatted: true,
                    hasBowled: false,
                },
                {
                    id: 13,
                    name: 'Prashant',
                    isBowler: true,
                    isOut: false,
                    runsTaken: 0,
                    ballsPlayed: 0,
                    foursPlayed: 0,
                    sixPlayed: 0,
                    strikeRate: 0.0,
                    wickets: 0,
                    runsGiven: 0,
                    maidens: 0,
                    oversBowled: 0,
                    extras :0,
                    onCrease: true,
                    hasBatted: true,
                    hasBowled: false,
                },
            ],
        }
    ],
    innings: [
        {
            id: 1,
            battingTeam: 1,
            bowlingTeam: 2,
            playersOnCrease: [1, 2],
            currentBatsman: 1,
            currentBowler: 14,
            oversPlayed: 20,
        },
        {
            id: 2,
            battingTeam: 2,
            bowlingTeam: 1,
            playersOnCrease: [12, 13],
            currentBatsman: 12,
            currentBowler: 4,
            oversPlayed: 3,
        }
    ],
    game: {
        winner: -1,
        inning1: 1,
        inning2: 2,
        currentInning: 2,
        isFirstInning : false,
        totalOvers: 20,
    },
}}

const mockStore = configureStore();

describe('<CurrentBallsScoreRecorder />', () => {
    it('renders without crashing', () => {
        let store = mockStore(initialData);
        const div = document.createElement('div');
        ReactDOM.render(<CurrentBallsScoreRecorder store={store} />, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    it('renders current player correctly', () => {
        let store = mockStore(initialData);
        const currentBallsScoreRecorder = shallow(<CurrentBallsScoreRecorder store={store}/>);
        expect(currentBallsScoreRecorder.html()).toEqual(
            expect.stringContaining('class="btn btn-secondary active">Abhijit')
          );
    });

    it('changes current batsman on click', () => {
        let store = mockStore(initialData);
        let mntWrapper = mount(<CurrentBallsScoreRecorder store={store}/>);
        let inactivePlayer = mntWrapper.find('button').filter({className:"btn btn-secondary"}).first();
        console.log('inactivePlayer - ', inactivePlayer.html());
        console.log('before click - ', mntWrapper.html());
        inactivePlayer.simulate('click');
        console.log('after click - ', mntWrapper.html());
        expect(mntWrapper.html()).toEqual(
            expect.stringContaining('class="btn btn-secondary active">Prashant')
          );
        // expect(mntWrapper.html()).toEqual(
        //     expect.stringContaining('class="btn btn-secondary">Abhijit')
        // );
    });
});
