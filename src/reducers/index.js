import { combineReducers } from 'redux';
import gameReducer from "./gameReducer";
import ScoreReducer from "./ScoreReducer";

const rootReducer = combineReducers({
    game: gameReducer,
    scoreReducer : ScoreReducer 
});

export default rootReducer;
