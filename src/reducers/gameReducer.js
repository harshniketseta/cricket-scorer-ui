import initialState from './initialData';
import {UPDATE_CURRENT_BATSMAN} from "../appConstants";
import jQuery from 'jquery';


const reducer = (state = initialState, action) => {
    switch(action.type) {
        case UPDATE_CURRENT_BATSMAN:
        let newState = jQuery.extend(true, {}, state);
        let currentInningId = newState.game.currentInning;
        let currentInning = newState.innings.filter((inning) => inning.id === currentInningId)[0];
        currentInning.currentBatsman = action.playerId;
        return newState;
        
        default: return state;
    }
    
};


export default reducer;
