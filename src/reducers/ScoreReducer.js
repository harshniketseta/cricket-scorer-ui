import initialState from './initialData';

const scoreReducer = (state = initialState, action) => {

    let currentInning = (innings, currentInningId) => {
        innings.find((inning) => {
            return currentInningId === inning.id;
        });
    }

    let currentBattingTeam = (teams, currentInning) => {
        teams.find((team) => {
            return team.id === currentInning.battingTeam;
        });
    }

    let updateScore = (state) => {
        var tempState = Object.create(state);
        var innings = tempState.game.innings;
        var currentInningId = tempState.game.game.currentInning;
        var teams = tempState.game.teams;


        let currentInningObj = currentInning(innings, currentInningId);
        let currentBattingTeamObj = currentBattingTeam(teams, currentInningObj);

        updatePlayerScore(teams,currentInning,currentBattingTeamObj);
        updateTeamScore(currentBattingTeamObj);

        return tempState;
    }

    let updatePlayerScore = (teams, currentInning, currentBattingTeam) => {

        var score = action.score;
        let currentBatsman = teams[currentBattingTeam.players[currentInning.currentBatsman]];
        currentBatsman.runsTaken += score;
        //for extras(wide and no ball), balls played to be excluded

        currentBatsman.ballsPlayed++;

        if (score === 4) {
            currentBatsman.foursPlayed++;
        }
        else if (score === 6) {
            currentBatsman.sixPlayed++;
        }

        currentBatsman.strikeRate = parseFloat(currentBatsman.runsTaken / currentBatsman.ballsPlayed);
    }

    let updateTeamScore = (currentBattingTeam) => {
        var score = action.score;
        currentBattingTeam.score += score;

    };

    switch (action.type) {
        case 'RECORD_SCORE':
            return [...state,updateScore(state)];
         default:
            return state;
    }

}
export default scoreReducer;