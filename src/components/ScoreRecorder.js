import React, { Component } from 'react';
import { recordScore } from '../actions';
import { connect } from 'react-redux';


const mapStateToProps = state => {
    // console.log("hello");
    return { state: state };
};

const mapDispatchToProps = dispatch => {
    return {
        onclick: score => dispatch(recordScore(score))

    };
};

class ScoreRecorder extends Component {

    constructor(props){
        super(props);
        this.state={score:0};
        this.updateScore =this.updateScore.bind(this);
    }

    updateScore(event){
        this.setState({
            score : event.target.value
        })
    }

    render() {
        return (

            <div>
                <div className="offset-lg-4 btn-group" role="group" aria-label="Basic example">
                    <button onClick={this.updateScore} type="button" className="btn btn-secondary" value='0'>0</button>
                    <button onClick={this.updateScore} type="button" className="btn btn-secondary" value='1'>1</button>
                    <button onClick={this.updateScore} type="button" className="btn btn-secondary" value='0'>2</button>
                    <button onClick={this.updateScore} type="button" className="btn btn-secondary" value='0'>3</button>
                    <button onClick={this.updateScore} type="button" className="btn btn-secondary" value='0'>4</button>
                    <button onClick={this.updateScore} type="button" className="btn btn-secondary" value='0'>5</button>
                    <button onClick={this.updateScore} type="button" className="btn btn-secondary" value='0'>6</button>
                    <button onClick={this.updateScore} type="button" className="btn btn-secondary" value='0'>7</button>
                    

                </div>

                <br />
                <br />

                <button className="offset-lg-4 btn-group" type="submit">Next Ball</button>
            </div>



        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ScoreRecorder);