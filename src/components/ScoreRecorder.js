import React,{Component} from 'react';
import {recordScore} from '../actions';
import {connect} from 'react-redux';


    const mapStateToProps = state => {
        // console.log("hello");
        return { state: state };
    };

   const mapDispatchToProps = dispatch => {
     return {
       onclick: score => dispatch(recordScore(score))
       
     };
   };

class ScoreRecorder extends Component{

    render(){
        return(

            <div>
            <div className="offset-lg-4 btn-group" role="group" aria-label="Basic example">
            <button type="button" className="btn btn-secondary">0</button>
            <button type="button" className="btn btn-secondary">1</button>
            <button type="button" className="btn btn-secondary">2</button>
            <button type="button" className="btn btn-secondary">3</button>
            <button type="button" className="btn btn-secondary">4</button>
            <button type="button" className="btn btn-secondary">5</button>
            <button type="button" className="btn btn-secondary">6</button>
            <button type="button" className="btn btn-secondary">7</button>
            </div>
            <br/>
            <br/>
            <button className="offset-lg-4 btn-group" type="submit">Next Ball</button>
            </div>


        );
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(ScoreRecorder);