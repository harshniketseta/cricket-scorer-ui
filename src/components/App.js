import React, { Component } from 'react';
import {connect} from "react-redux";
import './App.css';
import UmpireAdminPage from './UmpireAdminPage'
import PlayerStatsPage from './PlayerStatsPage'

import {
  Carousel,
  CarouselItem,
  CarouselIndicators,
} from 'reactstrap';

const pages = [
  {
    id: 1,
    pageComponents: <UmpireAdminPage />,
    caption: 'Umpire Admin'
  },
  {
    id: 2,
    pageComponents: <PlayerStatsPage />,
    caption: 'Player Stats'
  }
];

export class App extends Component {
  constructor(props) {
    super(props);
    this.state = { activeIndex: 0 };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === pages.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? pages.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }

  render() {
    const { activeIndex } = this.state;
    const slides = pages.map((page) => {
      return (
        <CarouselItem className="custom-tag" tag="div"
          key={page.id}
          onExiting={this.onExiting}
          onExited={this.onExited}
        >
          {page.pageComponents}
        </CarouselItem>
      );
    });

    return (
      <Carousel
        activeIndex={activeIndex}
        next={this.next}
        previous={this.previous}
        interval={false}
      >
        <CarouselIndicators items={pages} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
        {slides}
      </Carousel>
    );
  }
}
const mapStateToProps = (state) => {
    return {
        game:state
    }
};

export default connect(
    mapStateToProps,
    null
)(App);
