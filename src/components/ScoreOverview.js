import React,{Component} from 'react';
import {connect} from 'react-redux';
import "./ScoreStyling.css";
import Score from "./Score";

class ScoreOverview extends Component{

    getInningObjectFromCurrentInning = (currentInningId,innings)=>{
        return innings.find((inning)=>{
               return currentInningId === inning.id;
        });
    };
    
    getTeamObjectFromInning = (teamId,teams)=>{
        return teams.find((team)=>{
               return team.id === teamId;
        });
    };

    render(){
        let game = this.props.gameState.game;
        let currentInning = this.getInningObjectFromCurrentInning(this.props.gameState.game.currentInning,this.props.gameState.innings);
        let currentTeam =  this.getTeamObjectFromInning(currentInning.battingTeam,this.props.gameState.teams); 
        let currentTeamStats = `${currentTeam.score}/${currentTeam.wickets} in ${currentInning.oversPlayed}/${game.totalOvers}`;
        let isFirstInning = game.isFirstInning;
        if(isFirstInning){
        return(
        <div className = "score-overview">
            <Score teamName={currentTeam.name} teamStats={currentTeamStats} bold={true} />
        </div>     
        );
        }
        else{
            let previousInning = this.getInningObjectFromCurrentInning(this.props.gameState.game.inning1,this.props.gameState.innings);
            let previousTeam =  this.getTeamObjectFromInning(previousInning.battingTeam,this.props.gameState.teams); 
            let previousTeamStats = `${previousTeam.score}/${previousTeam.wickets} in ${previousInning.oversPlayed}`;
        return(
            <div className = "score-overview">
                <Score teamName={currentTeam.name} teamStats={currentTeamStats} bold={true} />
                <Score teamName={previousTeam.name} teamStats={previousTeamStats} />
            </div>
            );
        }
    }
    
}

const mapStateToProps = state=>{
    return {gameState : state.game};
};

export default connect(mapStateToProps)(ScoreOverview);