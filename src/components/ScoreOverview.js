import React,{Component} from 'react';
import {connect} from 'react-redux';
import "./ScoreStyling.css"

class ScoreOverview extends Component{

    getInningObjectFromCurrentInning = (currentInningId,innings)=>{
        return innings.find((inning)=>{
               return currentInningId === inning.id;
        });
    };
    
    getTeamObjectFromInning = (teamId,teams)=>{
        return teams.find((team)=>{
               return team.id === teamId;
        });
    };

    render(){
        let game = this.props.gameState.game;
        let currentInning = this.getInningObjectFromCurrentInning(this.props.gameState.game.currentInning,this.props.gameState.innings);
        let currentTeam =  this.getTeamObjectFromInning(currentInning.battingTeam,this.props.gameState.teams); 
        let currentTeamStats = `${currentTeam.score}/${currentTeam.wickets} in ${currentInning.oversPlayed}/${game.totalOvers}`;
        let isFirstInning = game.isFirstInning;
        if(isFirstInning){
        return(
        <div className = "score-overview">
            <div className="row">
                    <div className="col-lg-6 col-sm-12 font-weight-bold">
                        <span className = "float-left">Team {currentTeam.name}</span>
                        <span className = "float-right">{currentTeamStats}</span>
                    </div>
                
            </div>  
        </div>     
        );
        }
        else{
            let previousInning = this.getInningObjectFromCurrentInning(this.props.gameState.game.inning1,this.props.gameState.innings);
            let previousTeam =  this.getTeamObjectFromInning(previousInning.battingTeam,this.props.gameState.teams); 
            let previousTeamStats = `${previousTeam.score}/${previousTeam.wickets} in ${previousInning.oversPlayed}`;
        return(
            <div className = "score-overview">
            <div className="row">
                <div className="offset-lg-3 col-lg-6 col-sm-12 font-weight-bold">
                    <span className = "float-left">Team {currentTeam.name}</span>
                    <span className = "float-right">{currentTeamStats}  
                    </span>
                </div>
            </div>
            <div className="row">  
                <div className="offset-lg-3 col-lg-6 col-sm-12">
                    <span className = "float-left">Team {previousTeam.name}</span>
                    <span className = "float-right">{previousTeamStats}</span>
                </div>           
            </div>
            </div>
            );
        }
    }
    
}

const mapStateToProps = state=>{
    return {gameState : state.scoreReducer};
};

export default connect(mapStateToProps)(ScoreOverview);