import React,{Component} from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {connect} from "react-redux";
import "./PlayerListModal.css"

export class PlayerListModal extends Component{
    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };

        this.toggle = this.toggle.bind(this);
        this.requestNextBowler = this.requestNextBowler.bind(this);
        this.selectNextBowler = this.selectNextBowler.bind(this);
    }
    
    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    requestNextBowler() {
        this.toggle();
    }

    generateBowlersList() {
        
        let bowlersList = this.props.bowlers.map((bowler, index) => {
            if(index === 0) {
                return(
                    <a className="list-group-item list-group-item-action active" key={index} id={bowler.id} data-toggle="list" href={"#list-bowler-"+bowler.name} aria-controls={bowler.name}>{bowler.name}</a>
                )
            } else {
                return(
                    <a className="list-group-item list-group-item-action" key={index} id={bowler.id} data-toggle="list" href={"#list-bowler-"+bowler.name} aria-controls={bowler.name}>{bowler.name}</a>
                )
            } 
        });
        return bowlersList;
    }

    selectNextBowler(){
        this.toggle();
    }

    render() {
        let bowlersList = this.generateBowlersList();
        return (
            <div>
            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                <ModalHeader toggle={this.toggle}>Bowlers</ModalHeader>
                <ModalBody>
                <div className="row">
                    <div className="col-12">
                        <div className="list-group" id="list-tab" role="tablist">
                            {bowlersList}
                        </div>
                    </div>
                </div>
                </ModalBody>
                <ModalFooter>
                <Button color="primary" onClick={this.selectNextBowler}>Choose</Button>
                </ModalFooter>
            </Modal>
            </div>
        );
    }
}

const mapStatsToProps = (state) => {
    let inning=state.game.innings.filter((inning) => inning.id === state.game.game.currentInning)[0];
    let bowlingTeam=state.game.teams.filter((team) => team.id === inning.bowlingTeam)[0];
    let bowlers=bowlingTeam.players.filter((player) => player.isBowler);
    return {
        bowlers: bowlers
    }
}

export default connect(mapStatsToProps, null, null, { withRef: true })(PlayerListModal);