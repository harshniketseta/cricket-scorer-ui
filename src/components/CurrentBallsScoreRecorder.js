import React from 'react';
import {connect} from "react-redux";
import { UPDATE_CURRENT_BATSMAN } from '../appConstants'

export class CurrentBallScoreRecorder extends React.Component {

    constructor(props){
        super(props);
        this.openScoreSelector = this.openScoreSelector.bind(this);
        this.getClassName = this.getClassName.bind(this);
    }

    openScoreSelector = (event,player) => {

        this.props.updateCurrentBatsman(player.id);
         var i;
         var buttons = document.getElementsByName('button');
        for(i=0;i<buttons.length;i++){
            buttons[i].className = buttons[i].className.replace(" active","");
        }
        event.target.className += " active";
    }

    getClassName(player) {
        let defaultName = 'btn btn-secondary';
        return player.id === this.props.currentBatsmanId ? defaultName + " active" : defaultName;
    }

    render() {
    return (<div className="row">
        <div className="offset-lg-3 col-lg-offset-3 col-lg-6 col-sm-12" align="center">
        <div className="btn-group" role="group">
            <button type="button" name='button' className={this.getClassName(this.props.player1)} onClick={(event) => this.openScoreSelector(event,this.props.player1)}>{this.props.player1.name}</button>
            <button type="button" name='button' className={this.getClassName(this.props.player2)} onClick={(event) =>this.openScoreSelector(event,this.props.player2)}>{this.props.player2.name}</button>
        </div>
        </div>
    </div>)
    }
}

const mapStatsToProps = state => {
    let inning=state.game.innings.filter((inning) => inning.id === state.game.game.currentInning)[0];
    let battingTeam=state.game.teams.filter((team) => team.id === inning.battingTeam)[0];
    let playersOnCrease=inning.playersOnCrease;
    let player1 = battingTeam.players.filter((player) => player.id === playersOnCrease[0])[0];
    let player2 = battingTeam.players.filter((player) => player.id === playersOnCrease[1])[0];
    let currentBatsmanId = inning.currentBatsman;
    return {
      player1: player1,
      player2: player2,
      currentBatsmanId: currentBatsmanId
   }
}

const mapDispatchToProps = dispatch => {
    return {
        updateCurrentBatsman: (playerId)=> dispatch({type: UPDATE_CURRENT_BATSMAN,playerId: playerId})
   }
}

export default connect(mapStatsToProps,mapDispatchToProps)(CurrentBallScoreRecorder);