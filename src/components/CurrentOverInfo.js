import React from 'react';
import {connect} from "react-redux";

export class CurrentOverInfo extends React.Component {
    render() {
        return (
            <div className="row">
                <div className="offset-lg-3 col-lg-6 col-sm-12">
                    This Over : {this.getOver()}
                </div>
            </div>
        );
    }
    getOver(){
        return this.props.currentOverInfo.map((ballInfo) => {
            return ballInfo.join(",")
        }).join(" ")
    }
}

const mapStatsToProps = state => {
    let inning=state.game.innings.filter((inning) => inning.id === state.game.game.currentInning)[0];
    return {
        currentOverInfo: inning.currentOverState,
    }
};

export default connect(mapStatsToProps)(CurrentOverInfo);