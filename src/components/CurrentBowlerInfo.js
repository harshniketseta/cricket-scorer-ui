import React from 'react';
import {connect} from "react-redux";

export class CurrentBowlerInfo extends React.Component
{
    render(){
        return (
            <div className="row">
                <div className="offset-lg-3 col-lg-offset-3 col-lg-6 col-sm-12">
                    Bowler : {this.props.currentBowler}
                </div>
            </div>
        );
    }
}

const mapStatsToProps = state => {
    let inning=state.game.innings.filter((inning) => inning.id === state.game.game.currentInning)[0];
    let bowlingTeam=state.game.teams.filter((team) => team.id === inning.bowlingTeam)[0];
    let bowlerName=bowlingTeam.players.filter((player) => player.id === inning.currentBowler)[0].name;
    return {
        currentBowler: bowlerName
    }
}

export default connect(mapStatsToProps)(CurrentBowlerInfo);