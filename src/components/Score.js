import React,{Component} from 'react';

export default class Score extends Component {
    render(){
        let boldClass = (this.props.bold)?' font-weight-bold':'';
        return(
            <div className="row">  
                <div className={"offset-lg-3 col-lg-6 col-sm-12" + boldClass}>
                    <span className = "float-left">Team {this.props.teamName}</span>
                    <span className = "float-right">{this.props.teamStats}</span>
                </div>
            </div>
        );

    }
}
