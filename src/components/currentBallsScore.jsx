import React from 'react';
import {connect} from "react-redux";

class CurrentBallScore extends React.Component {

    constructor(props){
        super(props);
        this.openScoreSelector = this.openScoreSelector.bind(this);
    }

    openScoreSelector = (event,scoreSelectorId) => {
        var i;
       // console.log("openscoreselector = > ",scoreSelectorId);
        var tabContents = document.getElementsByClassName('tabContent');
       // console.log("tab contents ",tabContents);
       for(i=0;i<tabContents.length;i++){
        tabContents[i].style.display = "none";
       }
        var tabLinks = document.getElementsByClassName('tabLinks');
        for(i=0;i<tabLinks.length;i++){
            tabLinks[i].className = tabLinks[i].className.replace(" active","");
            tabLinks[i].style.background = "";
        }
        document.getElementById(scoreSelectorId).style.display = "block";
        event.target.className += " active";
        event.target.style.background="lightblue";
    }


    render() {
    return (<div>
        <div class="tab">
            <button class="tabLinks"  style={{background:'lightblue'}}  onClick={(event) => this.openScoreSelector(event,'player1ScoreSelect')}>{this.props.player1}</button>
             <button class="tabLinks" onClick={(event) => this.openScoreSelector(event,'player2ScoreSelect')}>{this.props.player2}</button>
        </div>
        <br></br>
        <div id="player1ScoreSelect" class="tabContent">
        <span class="btn-group">
       <button type="button" class="btn">0</button>
       <button type="button" class="btn">1</button>
       <button type="button" class="btn">2</button>
       <button type="button" class="btn">3</button>
       <button type="button" class="btn">4</button>
       <button type="button" class="btn">5</button>
       <button type="button" class="btn">6</button>
       <button type="button" class="btn">7</button>
       </span>
        </div>

        <div id="player2ScoreSelect" class="tabContent" style={{display:'none'}}>
            <p>hi</p>
        </div>
    </div>)
    }
}

const mapStatsToProps = state => {
    let inning=state.game.innings.filter((inning) => inning.id === state.game.game.currentInning)[0];
    let player1 = state.game.players.filter((player) => player.id === inning.playersOnCrease[0])[0].name;
    let player2 = state.game.players.filter((player) => player.id === inning.playersOnCrease[1])[0].name;

   return {
      player1: player1,
      player2: player2 
   }
}

export default connect(mapStatsToProps)(CurrentBallScore);