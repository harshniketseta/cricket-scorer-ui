import React from 'react';
import {connect} from "react-redux";
import CurrentOverInfo from "./CurrentOverInfo";
import CurrentBowlerInfo from "./CurrentBowlerInfo";
import ScoreOverview from './ScoreOverview';
import SpacingRow from "./SpacingRow";
import CurrentBallsScoreRecorder from './CurrentBallsScoreRecorder';
import ScoreRecorder from "./ScoreRecorder";
import PlayerListModal from "./PlayerListModal";
import { Button } from 'reactstrap';

class UmpireAdminPage extends React.Component {
    constructor(props) {
        super(props);
        this.playerModal = React.createRef();
        this.togglePlayerModal = this.togglePlayerModal.bind(this);
    }
    render(){
        return(
            <div className="page">
                <div className="container-fluid">
                    <SpacingRow />
                    <ScoreOverview route = "main"/> 
                    <SpacingRow />
                    <CurrentOverInfo />
                    <CurrentBowlerInfo />
                    <SpacingRow />
                    <SpacingRow />
                    <CurrentBallsScoreRecorder />
                    <SpacingRow />
                    <ScoreRecorder />
                    <PlayerListModal ref={connectedComponent => {
                            this.playerModal = connectedComponent ? connectedComponent.getWrappedInstance() : null;
                        }
                    } />
                    <Button color="danger" onClick={this.togglePlayerModal}>Demo Bowlers Modal</Button>
                </div>
            </div>
        )
    };
    togglePlayerModal() {
        this.playerModal.requestNextBowler();
    }
    componentDidMount() {
        document.querySelectorAll('.page').forEach(el => el.style.minHeight = window.innerHeight + 'px');
    }
}

const mapStateToProps = (state) => {
    return {
        state:state
    }
};

export default connect(
    mapStateToProps,
    null
)(UmpireAdminPage);