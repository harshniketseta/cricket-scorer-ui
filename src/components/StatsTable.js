import React from 'react';
import {connect} from "react-redux";
import "./StatsTable.css";

export class StatsTable extends React.Component
{
    render(){
        const headerColumns = this.props.columns.map((column) => {
            return (
                <th scope="col">{column}</th>
              );
            });
        const data = this.props.players.map((player) => {
            return this.renderRow(player);
        });
        return (
            <div className="row statsTable">
                <div className="offset-lg-3 col-lg-6 col-sm-12">
                    <h5 className="card-title">{this.props.title}</h5>
                    <table className="table">
                        <thead>
                            <tr>
                                {headerColumns}
                            </tr>
                        </thead>
                        <tbody>
                            {data}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    };
    renderColumns(player){
        let columns = this.props.keys.map((key) => {
            let data = player[key];
            if( this.props.team.isBatting && key === 'name' && player.onCrease ) {
                return (
                    <th scope="row">{data}<sup>*</sup></th>
                );
            } else {
                return (
                    <td>{data}</td>
                );    
            } 
        });
        return columns;
    }
    renderRow(player){
        return (
            <tr>{this.renderColumns(player)}</tr>
        );
    }
}

const mapStatsToProps = (state,OwnProps) => {
    return {
        title: OwnProps.title,
        players: OwnProps.players,
        columns: OwnProps.columns,
        keys: OwnProps.keys,
        team: OwnProps.team,
    }
}

export default connect(mapStatsToProps)(StatsTable);