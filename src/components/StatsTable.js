import React from 'react';
import {connect} from "react-redux";
import "./StatsTable.css";

export class StatsTable extends React.Component
{
    render(){
        const headerColumns = this.props.columns.map((column, index) => {
            return (
                <th key={index} scope="col">{column}</th>
              );
            });
        const data = this.props.players.map((player, index) => {
            return this.renderRow(player, index);
        });
        return (
            <div className="row statsTable">
                <div className="offset-lg-3 col-lg-6 col-sm-12">
                    <h5 className="card-title">{this.props.title}</h5>
                    <table className="table">
                        <thead>
                            <tr>
                                {headerColumns}
                            </tr>
                        </thead>
                        <tbody>
                            {data}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    };
    renderColumns(player){
        let columns = this.props.keys.map((key, index) => {
            let data = player[key];
            if( this.props.team.isBatting && key === 'name' && player.onCrease ) {
                return (
                    <th key={index} scope="row">{data}<sup>*</sup></th>
                );
            } else {
                return (
                    <td key={index}>{data}</td>
                );    
            } 
        });
        return columns;
    }
    renderRow(player, index){
        return (
            <tr key={index}>{this.renderColumns(player)}</tr>
        );
    }
}

const mapStatsToProps = (state,OwnProps) => {
    return {
        title: OwnProps.title,
        players: OwnProps.players,
        columns: OwnProps.columns,
        keys: OwnProps.keys,
        team: OwnProps.team,
    }
}

export default connect(mapStatsToProps)(StatsTable);