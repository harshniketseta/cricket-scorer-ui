import React from 'react';
import {connect} from "react-redux";
import ScoreOverview from './ScoreOverview';
import StatsTable from './StatsTable';
import SpacingRow from "./SpacingRow";

class PlayerStatsPage extends React.Component {
    render(){
        return(
            <div className="page">
                <div className="container-fluid">
                    <SpacingRow />
                    <ScoreOverview route = "detail"/>
                    <SpacingRow />
                    <StatsTable 
                        title="Batting Table" 
                        team={this.getBattingTeam()} 
                        players={this.getBattingPlayers()} 
                        columns={this.getBattingStatColumns()} 
                        keys={this.getBattingStatKeys()}
                    />

                    <StatsTable 
                        title="Bowling Table" 
                        team={this.getBowlingTeam()} 
                        players={this.getPlayersBowled()} 
                        columns={this.getBowlingStatColumns()} 
                        keys={this.getBowlingStatKeys()}
                    />
                </div>
            </div>
        )
    };
    getBattingTeam(){
        return this.props.battingTeam;
    }
    getBattingPlayers(){
        return this.props.battingTeam.players.filter((player) => player.hasBatted );
    }
    getBattingStatColumns(){
        return ["Batsman", "Runs", "Balls", "Fours", "Sizes", "Strike Rate"];
    }
    getBattingStatKeys(){
        return ["name", "runsTaken", "ballsPlayed", "foursPlayed", "sixPlayed", "strikeRate"];
    }
    getBowlingTeam(){
        return this.props.bowlingTeam;
    }
    getPlayersBowled(){
        return this.props.bowlingTeam.players.filter((player) => player.hasBowled );
    }
    getBowlingStatColumns(){
        return ["Bowler", "Overs", "Maiden", "Run", "Wickets", "Extras"];
    }
    getBowlingStatKeys(){
        return ["name", "oversBowled", "maidens", "runsGiven", "wickets", "extras"];
    }
    componentDidMount() {
        document.querySelectorAll('.page').forEach(el => el.style.minHeight = window.innerHeight + 'px');
    }
}

const mapStatsToProps = (state) => {
    let inning=state.game.innings.filter((inning) => inning.id === state.game.game.currentInning)[0];
    let battingTeam=state.game.teams.filter((team) => team.id === inning.battingTeam)[0];
    let bowlingTeam=state.game.teams.filter((team) => team.id === inning.bowlingTeam)[0];
    return {
        battingTeam: battingTeam,
        bowlingTeam: bowlingTeam,
    }
}

export default connect(
    mapStatsToProps
)(PlayerStatsPage);