export const recordScore= (score) => ({
    type : 'RECORD_SCORE',
    score
});